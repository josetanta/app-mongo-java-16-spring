package org.systemia.MongoApp.mongoconfig;


import org.springframework.boot.CommandLineRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.systemia.MongoApp.models.Address;
import org.systemia.MongoApp.models.Gender;
import org.systemia.MongoApp.models.Student;
import org.systemia.MongoApp.repository.StudentRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class MongoConfig {
    String email = "jose@mail.com";

    public CommandLineRunner runner(StudentRepository repository, MongoTemplate mongoTemplate) {
        return args -> {
            var address = new Address("Perú", "Ayacucho", "05002");
            var student = new Student(
                    "Jose",
                    "Tanta",
                    "jose@mail.com",
                    Gender.MALE,
                    address,
                    List.of("Computer Science", "Engendering Software"),
                    BigDecimal.TEN
            );
            // usingMongoTemplateAndQuery(repository, mongoTemplate, student);
            repository.findStudentByEmail(email)
                    .ifPresentOrElse(s -> {
                        System.out.println(s + " already exists");
                    }, () -> {
                        System.out.println("Inserting student " + student);
                        repository.insert(student);
                    });

        };
    }

    private void usingMongoTemplateAndQuery(StudentRepository repository, MongoTemplate mongoTemplate, Student student) {
        Query query = new Query();

        query.addCriteria(Criteria.where("email").is(email));

        var students = mongoTemplate.find(query, Student.class);

        if (students.size() > 1) {
            throw new IllegalStateException("Found many students with email " + email);
        }

        if (students.isEmpty()) {
            System.out.println("Inserting student " + student);
            repository.insert(student);
        } else {
            System.out.println(student + " already exists");
        }
    }
}