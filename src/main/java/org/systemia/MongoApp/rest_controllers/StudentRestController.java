package org.systemia.MongoApp.rest_controllers;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.systemia.MongoApp.models.Student;
import org.systemia.MongoApp.services.StudentService;

import java.util.List;

@RestController
@RequestMapping("api/students")
@AllArgsConstructor
public class StudentRestController {

    @Autowired
    private final StudentService studentService;

    @GetMapping
    public ResponseEntity<List<Student>> fetchAllStudents() {
        return new ResponseEntity<>(studentService.getAllStudents(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Student> registerStudent(@RequestBody Student student) {
        return new ResponseEntity<>(studentService.registerStudent(student), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "{studentId}")
    public ResponseEntity<Object> deleteStudent(@PathVariable String studentId) {
        studentService.deleteStudent(studentId);
        return new ResponseEntity<>("Delete Student", HttpStatus.OK);
    }
}