package org.systemia.MongoApp.services;

import org.systemia.MongoApp.models.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudents();

    Student registerStudent(Student student);

    boolean deleteStudent(String studentId);
}