package org.systemia.MongoApp.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.systemia.MongoApp.models.Student;
import org.systemia.MongoApp.repository.StudentRepository;

import java.util.List;

@Service("studentService")
@Transactional
@AllArgsConstructor
public class StudentServiceImpl implements StudentService {

    @Autowired
    private final StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student registerStudent(Student student) {
        return studentRepository.insert(student);
    }

    @Override
    public boolean deleteStudent(String studentId) {
        studentRepository.deleteById(studentId);
        return true;
    }
}