package org.systemia.MongoApp.models;

public enum Gender {
    MALE,
    FEMALE
}